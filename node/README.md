# Node
## Requirement
This project requires Node.

No specific version required, try to use a small node image.

## How to run
```bash
node hello-world.js
```
This will display a hello world message.

les commandes utiliser :

docker build -t my-nodejs-app .
docker run -it --rm --name my-running-app my-nodejs-app
