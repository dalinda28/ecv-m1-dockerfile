public class HelloWorld {
    public static void main(String[] args) throws Exception {
        String version = System.getProperty("java.specification.version");

        if (Double.parseDouble(version) == 1.7) {
            System.out.println("Hello world!");

            return;
        }

        System.err.println("Error I am not in Java 7: " + version);
    }
}
