# Java 7
## Requirement
This project requires Java 7.

## How to run
```bash
javac HelloWorld.java
java HelloWorld
```
This will display a hello world message if executed with Java 7 and an error message otherwise.

les commandes utiliser :

docker build -t my-java-app .
docker run -it --rm --name my-running-app my-java-app
