# PHP 8
## Requirement
This project requires PHP 8.

## How to run
```bash
php hello-world.php
```
This will display a hello world message if executed with PHP 8 and an error message otherwise.

les commandes utiliser :
docker build -t dalinda/php8 .
docker run dalinda/php8
